# Exercício Programado 2

## Introdução

No mundo de Pokémon, existem criaturas chamadas Pokémons. Estas criaturas possuem
diversas caracterísiticas e atributos.

Os Pokémons se organizam em tipos, e podem ser capturados por treinadores.

Neste Exercício Programado, foi feita uma aplicação mobile, para fazer com que os treinadores registrem seus pokemons.

## Objetivo

Construir uma Pokédex simples. Onde os treinadores possam se cadastrar,
e cadastrar os seus Pokémons. E possam ver as características destes Pokémons.

## Instalação

* Para instalar o App no seu  celular, você deve entrar neste [link](https://drive.google.com/open?id=1mleoXzlyTioFk9O49pezkiMAOV0-ztZ6) (Com seu celular) e fazer o download.

* Ao fazer o download acesse o arquivo com um explorador de arquivos e clique no mesmo.

* Apos isso você deverá entrar na tela de instalação você deverá permitir a instalação de aplicativos de outras fontes.

## Utilização

* Ao entrar no aplicativo, será aberta a tela de login, caso não possuir um cadastro você deverá clicar no texto abaixo do botão de login, sendo aberta a tela de cadastro do usuário.

* Dentro da tela de cadastro do usuário escolha seu gênero, digite seu email, nome e senha, caso o cadastro ocorrer com sucesso, você será redirecionado para a tela principal do app.

* Dentro da tela principal, existem 3 fragmentos de tela, as quais são: Pokedex, Perfil, Comunidade.

* Na tela da Pokedex, você terá acesso a quase todos os pokemons, assim como a seus tipos. Na mesma você tem a possibilidade de pesquisar os pokemons pelo nome ao clicar na lupa e escrever o nome do pokemon, assim como pesquisar os pokemons pelo tipo, selecionando a seta a qual ira demonstrar todos os tipos disponíveis, após selecionar a fileira de tipos, selecione o tipo específico que deseja. Selecionando o pokemon você será redirecionado para a tela de detalhes do pokemon.

* Na tela de detalhes do pokemon você terá acesso aos ataques do pokemon assim como a sua versão shiny, vindo da tela de pokedex, o botão de adcionar o pokemon aparecerá, e clicando no mesmo, caso você ainda não tenha o pokemon, ele será adcionado, caso você já tenha irá aparecer uma dialog perguntando se deseja adcionar novamente o pokemon.

* Na tela do Perfil você terá acesso aos seus pokemons adcionados, assim como a seu nome e seu genero, ao clicar no pokemon você irá novamente pra tela de detalhes do pokemon, todavia o botão adcionar será substituido pelo remover, podendo assim remover um certo pokemon.

* Na tela da Comunidade você terá acesso a uma lista com todos os treinadores registrados no app, mostrando seus nomes, generos e quantidade de pokemons, na mesma tela é possível pesquisar treinadores pelo nome. Ao clicar no treinador, você será redirecionado para a tela de detalhes do treinador.
 
* A tela de detalhes do treinador é parecida com a do perfil, porém com os detalhes do treinador selecionado, e com seus pokemons, ao clicar no pokemon desse treinador você será redirecionado para a tela de detalhes do pokemon, exatamente igual a da de quando sai da pokedex para a mesma.

* Para fazer logout clique nos 3 pontos no canto superior direito.

## Problemas

* As versões de Alola não aparecerão, devido o fato de, até o momento da produção desse app, a api utilizada não possuí suas imagens.

* Os pokemons demoram entre 2min e 50s para carregar, depedendo do seu celular, se utilizar a pesquisa pelo nome o app pode engasgar.

## Metodologia

* Feito com android studio.

* Autenticação e banco de dados no firebase.

## Bibliografia

[https://developer.android.com/](https://developer.android.com/)

[https://firebase.google.com/docs/android/setup](https://firebase.google.com/docs/android/setup)

[https://stackoverflow.com/](https://stackoverflow.com/)

