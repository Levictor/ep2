package com.example.levictor.pokedex.View.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.levictor.pokedex.View.Activities.PokemonDetalhesActivity;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.PokemonTypes;
import com.example.levictor.pokedex.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.example.levictor.pokedex.Helper.SystemConstants.BUG_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.DARK_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.DRAGON_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.ELECTRIC_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.FAIRY_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.FIGHTING_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.FIRE_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.FLYING_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.GHOST_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.GRASS_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.GROUND_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.ICE_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.NORMAL_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.NUMERO_MAXIMO_POKEMONS;
import static com.example.levictor.pokedex.Helper.SystemConstants.POISON_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.PSYCHIC_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.ROCK_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.STEEL_TYPE;
import static com.example.levictor.pokedex.Helper.SystemConstants.WATER_TYPE;

public class ListaPokemonAdapter extends RecyclerView.Adapter<ListaPokemonAdapter.ViewHolder>{
  private ArrayList<Pokemon> dataset;
  private Context mContext;
  private int size = 50;

  private String fonte;
  
  public ListaPokemonAdapter(Context context, String fonte){
    dataset = new ArrayList<>();
    this.mContext = context;
    this.fonte = fonte;
  }
  
  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i){
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pokemon, viewGroup, false);
    return new ViewHolder(view);
  }
  
  @Override
  public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i){
    try{
      final Pokemon p = dataset.get(i);
      String nomePokemon = tratarNomes(p.getName());
      viewHolder.nomePokemon.setText(nomePokemon);
      
      Glide.with(mContext)
           .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getId() + ".png")
           .centerCrop()
           .crossFade()
           .diskCacheStrategy(DiskCacheStrategy.ALL)
           .into(viewHolder.imagemPokemon);
      
      viewHolder.imagemPokemon.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v){
          Intent intent = new Intent(mContext, PokemonDetalhesActivity.class);
          intent.putExtra("idPokemon", p.getId());
          intent.putExtra("fonte", fonte);
          mContext.startActivity(intent);
        }
      });
      
      setTypes(p.getTypes(),viewHolder.tvTipo1,viewHolder.tvTipo2);
      
      if(p.getTypes().size() == 2)
        viewHolder.tvTipo2.setVisibility(View.VISIBLE);
      else
        viewHolder.tvTipo2.setVisibility(View.GONE);
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  
  private void setTypes(ArrayList<PokemonTypes> types, TextView tvTipo1, TextView tvTipo2){
    TextView tvTipo;

    for(PokemonTypes p: types){
      
      if(p.getSlot() == 1){
        tvTipo = tvTipo1;
      }
      else{
        tvTipo = tvTipo2;
      }
      
      switch(p.getType().getName()){
        case BUG_TYPE:
          tvTipo.setBackgroundResource(R.drawable.bug_type);
          break;
        case DARK_TYPE:
          tvTipo.setBackgroundResource(R.drawable.dark_type);
          break;
        case DRAGON_TYPE:
          tvTipo.setBackgroundResource(R.drawable.dragon_type);
          break;
        case ELECTRIC_TYPE:
          tvTipo.setBackgroundResource(R.drawable.electric_type);
          break;
        case FAIRY_TYPE:
          tvTipo.setBackgroundResource(R.drawable.fairy_type);
          break;
        case FIGHTING_TYPE:
          tvTipo.setBackgroundResource(R.drawable.fighting_type);
          break;
        case FIRE_TYPE:
          tvTipo.setBackgroundResource(R.drawable.fire_type);
          break;
        case FLYING_TYPE:
          tvTipo.setBackgroundResource(R.drawable.flying_type);
          break;
        case GHOST_TYPE:
          tvTipo.setBackgroundResource(R.drawable.ghost_type);
          break;
        case GRASS_TYPE:
          tvTipo.setBackgroundResource(R.drawable.grass_type);
          break;
        case GROUND_TYPE:
          tvTipo.setBackgroundResource(R.drawable.ground_type);
          break;
        case ICE_TYPE:
          tvTipo.setBackgroundResource(R.drawable.ice_type);
          break;
        case NORMAL_TYPE:
          tvTipo.setBackgroundResource(R.drawable.normal_type);
          break;
        case POISON_TYPE:
          tvTipo.setBackgroundResource(R.drawable.poison_type);
          break;
        case PSYCHIC_TYPE:
          tvTipo.setBackgroundResource(R.drawable.psychic_type);
          break;
        case ROCK_TYPE:
          tvTipo.setBackgroundResource(R.drawable.rock_type);
          break;
        case STEEL_TYPE:
          tvTipo.setBackgroundResource(R.drawable.steel_type);
          break;
        case WATER_TYPE:
          tvTipo.setBackgroundResource(R.drawable.water_type);
          break;
      }
      String newType = tratarNomes(p.getType().getName());
      tvTipo.setText(newType);
    }
  }
  
  @Override
  public int getItemCount(){
    return dataset.size();
  }
  
  public void adcionarPokemon(Pokemon pokemon){
    
    try{
      dataset.add(pokemon);
      if(dataset.size() == size || dataset.size() == NUMERO_MAXIMO_POKEMONS){
        Collections.sort(dataset, new Comparator<Pokemon>(){
          @Override
          public int compare(Pokemon o1, Pokemon o2){
            return o1.getId() - o2.getId();
          }
        });
        notifyDataSetChanged();
        size += 50;
      }
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  
  public void adcionarPokemon(ArrayList<Pokemon> listaPokemon){
    try{
      dataset = new ArrayList<>();
      dataset.addAll(listaPokemon);
      notifyDataSetChanged();
      
      Collections.sort(dataset, new Comparator<Pokemon>(){
        @Override
        public int compare(Pokemon o1, Pokemon o2){
          return o1.getId() - o2.getId();
        }
      });
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  
  public class ViewHolder extends RecyclerView.ViewHolder{
    private ImageView imagemPokemon;
    private TextView nomePokemon;
    private TextView tvTipo1;
    private TextView tvTipo2;
    
    public ViewHolder(@NonNull View itemView){
      super(itemView);
      imagemPokemon = itemView.findViewById(R.id.img_pokemon);
      nomePokemon = itemView.findViewById(R.id.txt_nome_pokemon);
      tvTipo1 = itemView.findViewById(R.id.tv_tipo1);
      tvTipo2 = itemView.findViewById(R.id.tv_tipo2);
      
    }
  }
  
  public static String tratarNomes(String text){
    char[] letters = text.toCharArray();
    letters[0] = Character.toUpperCase(letters[0]);
    for(int i = 0; i < letters.length; i++){
      if(letters[i] == '-'){
        if(!text.equals("ho-oh"))
          letters[i] = ' ';
        
        letters[i + 1] = Character.toUpperCase(letters[i + 1]);
      }
    }
    return new String(letters);
    
  }
  
}
