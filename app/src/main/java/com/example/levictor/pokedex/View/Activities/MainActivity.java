package com.example.levictor.pokedex.View.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.levictor.pokedex.View.Fragments.ComunidadeFragment;
import com.example.levictor.pokedex.View.Fragments.PokemonsFragment;
import com.example.levictor.pokedex.View.Fragments.TreinadorFragment;
import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.R;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity{
  
  private FirebaseAuth auth;
  
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener(){
    
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){
      Fragment fragment = null;
      switch(item.getItemId()){
        case R.id.navigation_home:
          fragment = new PokemonsFragment();
          break;
        
        case R.id.navigation_dashboard:
          fragment = new TreinadorFragment();
          break;
        
        case R.id.navigation_notifications:
          fragment = new ComunidadeFragment();
          break;
      }
      if(fragment != null){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
      }
      
      return true;
    }
  };
  
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    auth = ConfiguracaoFirebase.getFirebaseAuth();
    
    FrameLayout fragmentContainer = findViewById(R.id.fragment_container);
    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PokemonsFragment()).commit();
    
    
    BottomNavigationView navigation = findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.top_bar, menu);
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item){
    
    switch(item.getItemId()){
      case R.id.item1:
        auth.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        return true;
      
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  
}
