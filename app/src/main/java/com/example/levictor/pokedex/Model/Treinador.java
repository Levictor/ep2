package com.example.levictor.pokedex.Model;

import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;

public class Treinador{
  private String nome;
  
  private String senha;
  
  private String email;
  private String id;
//  NUMEROS DO GENERO:
// 2 FEMININO
// 4 MASCULINO
  private int genero;
  private ArrayList<Pokemon> pokemons;
  
  public Treinador(){
  }
  
  public int getGenero(){
    return genero;
  }
  
  public void setGenero(int genero){
    this.genero = genero;
  }
  
  @Exclude
  public String getSenha(){
    return senha;
  }
  
  @Exclude
  public void setSenha(String senha){
    this.senha = senha;
  }
  
  public String getEmail(){
    return email;
  }
  
  public void setEmail(String email){
    this.email = email;
  }
  
  public String getId(){
    return id;
  }
  
  public void setId(String id){
    this.id = id;
  }

  public String getNome(){
    return nome;
  }
  
  public void setNome(String nome){
    this.nome = nome;
  }
  
  public ArrayList<Pokemon> getPokemons(){
    return pokemons;
  }
  
  public void setPokemons(ArrayList<Pokemon> pokemons){
    this.pokemons = pokemons;
  }
  
  public void salvar(){
    DatabaseReference referenciaDatabase = ConfiguracaoFirebase.getFirebase();
    referenciaDatabase.child("Treinadores").child(getId()).setValue(this);
  }
}
