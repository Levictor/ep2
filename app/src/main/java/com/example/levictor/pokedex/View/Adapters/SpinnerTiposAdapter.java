package com.example.levictor.pokedex.View.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.levictor.pokedex.R;

import java.util.ArrayList;

public class SpinnerTiposAdapter extends ArrayAdapter{
  
  private static final String TAG = "POKEDEX";
  private String type;
  
  public SpinnerTiposAdapter(@NonNull Context context, ArrayList<String> objects){
    super(context, 0, objects);
  }
  
  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
//    Log.i(TAG, ""+position);
    return initView(position, convertView, parent);
  }
  
  @Override
  public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
    return initView(position, convertView, parent);
  }
  
  private View initView(int position, View convertView, ViewGroup parent){
    if(convertView == null){
      convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner, parent, false);
    }
    
    TextView tv_tipo_1 = convertView.findViewById(R.id.tv_spinner_tipo_1);
    TextView tv_tipo_2 = convertView.findViewById(R.id.tv_spinner_tipo_2);
    TextView tv_tipo_3 = convertView.findViewById(R.id.tv_spinner_tipo_3);
    
    switch(position){
      case 0:
        tv_tipo_1.setText("Normal");
        tv_tipo_1.setBackgroundResource(R.drawable.normal_type);
        
        tv_tipo_2.setText("Fighting");
        tv_tipo_2.setBackgroundResource(R.drawable.fighting_type);
        
        tv_tipo_3.setText("Flying");
        tv_tipo_3.setBackgroundResource(R.drawable.flying_type);
        
        break;
      
      case 1:
        tv_tipo_1.setText("Poison");
        tv_tipo_1.setBackgroundResource(R.drawable.poison_type);
        
        tv_tipo_2.setText("Ground");
        tv_tipo_2.setBackgroundResource(R.drawable.ground_type);
        
        tv_tipo_3.setText("Rock");
        tv_tipo_3.setBackgroundResource(R.drawable.rock_type);
        
        break;
      
      case 2:
        tv_tipo_1.setText("Bug");
        tv_tipo_1.setBackgroundResource(R.drawable.bug_type);
        
        tv_tipo_2.setText("Ghost");
        tv_tipo_2.setBackgroundResource(R.drawable.ghost_type);
        
        tv_tipo_3.setText("Steel");
        tv_tipo_3.setBackgroundResource(R.drawable.steel_type);
        
        break;
      case 3:
        tv_tipo_1.setText("Fire");
        tv_tipo_1.setBackgroundResource(R.drawable.fire_type);
        
        tv_tipo_2.setText("Water");
        tv_tipo_2.setBackgroundResource(R.drawable.water_type);
        
        tv_tipo_3.setText("Grass");
        tv_tipo_3.setBackgroundResource(R.drawable.grass_type);
        
        break;
      case 4:
        tv_tipo_1.setText("Electric");
        tv_tipo_1.setBackgroundResource(R.drawable.electric_type);
        
        tv_tipo_2.setText("Psychic");
        tv_tipo_2.setBackgroundResource(R.drawable.psychic_type);
        
        tv_tipo_3.setText("Ice");
        tv_tipo_3.setBackgroundResource(R.drawable.ice_type);
        
        break;
      case 5:
        tv_tipo_1.setText("Dragon");
        tv_tipo_1.setBackgroundResource(R.drawable.dragon_type);
        
        tv_tipo_2.setText("Dark");
        tv_tipo_2.setBackgroundResource(R.drawable.dark_type);
        
        tv_tipo_3.setText("Fairy");
        tv_tipo_3.setBackgroundResource(R.drawable.fairy_type);
        
        break;
    }
    return convertView;
  }
  
  public String getType(){
    return type;
  }
  
  public void setType(String type){
    this.type = type;
  }
}
