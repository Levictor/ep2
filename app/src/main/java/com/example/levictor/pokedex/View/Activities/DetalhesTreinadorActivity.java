package com.example.levictor.pokedex.View.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;
import com.example.levictor.pokedex.View.Adapters.ListaPokemonAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.levictor.pokedex.Helper.SystemConstants.ACTIVITY_DETALHES_TREINADOR;

public class DetalhesTreinadorActivity extends AppCompatActivity{
  public ArrayList<Pokemon> listaPokemons;
  private RecyclerView recyclerView;
  private ImageView imgGenero;
  private TextView tvNome;
  private ListaPokemonAdapter adapter;
  private DatabaseReference databaseReferenceTreinador;
  private DatabaseReference databaseReferencePokemons;
  private ValueEventListener valueEventListenerTreinadores;
  private ValueEventListener valueEventListenerPokemon;
  private String idTreinador;
  
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detalhes_treinador);
    
    initView();
    
    ActionBar actionBar = getSupportActionBar();
    assert actionBar != null;
    actionBar.hide();
    
    Intent intent = getIntent();
    if(intent.getExtras() != null){
      idTreinador = intent.getStringExtra("idTreinador");
    }
    
    listaPokemons = new ArrayList<>();
    getTreinador();
    getPokemons();
    adapter = new ListaPokemonAdapter(this, ACTIVITY_DETALHES_TREINADOR);
    
    recyclerView.setAdapter(adapter);
    recyclerView.setHasFixedSize(true);
    GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
    recyclerView.setLayoutManager(layoutManager);
  }
  
  private void initView(){
    imgGenero = findViewById(R.id.img_genero_perfil);
    tvNome = findViewById(R.id.tv_nome_treinador_perfil);
    recyclerView = findViewById(R.id.rv_treinador);
  }
  
  private void getPokemons(){
    databaseReferencePokemons = ConfiguracaoFirebase.getFirebase().child("Treinadores").child(idTreinador);
    
    valueEventListenerPokemon = new ValueEventListener(){
      @Override
      public void onDataChange(DataSnapshot dataSnapshot){
        
        listaPokemons.clear();
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            Pokemon pokemon = data.getValue(Pokemon.class);
            
            listaPokemons.add(pokemon);
          }
          catch(Exception e){
            e.printStackTrace();
          }
        }
        adapter.adcionarPokemon(listaPokemons);
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    databaseReferencePokemons.addListenerForSingleValueEvent(valueEventListenerPokemon);
  }
  
  private void getTreinador(){
    databaseReferenceTreinador = ConfiguracaoFirebase.getFirebase().child("Treinadores");
    
    valueEventListenerTreinadores = new ValueEventListener(){
      @Override
      public void onDataChange(DataSnapshot dataSnapshot){
        
        listaPokemons.clear();
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            Treinador treinador = data.getValue(Treinador.class);
            if(treinador.getId().equals(idTreinador)){
              if(treinador.getGenero() == 4){
                imgGenero.setBackgroundResource(R.drawable.character_male);
              }
              else if(treinador.getGenero() == 2){
                imgGenero.setBackgroundResource(R.drawable.character_female);
              }
              tvNome.setText(treinador.getNome());
            }
          }
          catch(Exception e){
            e.printStackTrace();
          }
        }
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    databaseReferenceTreinador.addValueEventListener(valueEventListenerTreinadores);
  }
  
  @Override
  protected void onStop(){
    super.onStop();
    databaseReferencePokemons.removeEventListener(valueEventListenerPokemon);
    databaseReferenceTreinador.removeEventListener(valueEventListenerTreinadores);
  }
}
