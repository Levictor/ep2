package com.example.levictor.pokedex.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.levictor.pokedex.Helper.Filtros;
import com.example.levictor.pokedex.View.Adapters.ListaTreinadoresAdapter;
import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Helper.UserPreferences;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class ComunidadeFragment extends Fragment{
  private SearchView searchView;
  private static final String TAG = "POKEDEX";
  private Context mContext;
  private ListaTreinadoresAdapter adapter;
  private ListView listView;
  private ArrayList<Pokemon> listaPokemons;
  private ArrayList<Treinador> listaTreinadores;
  private DatabaseReference firebaseReference;
  private ValueEventListener valueEventListenerTreinadores;
  private Filtros filtros;
  
  public ComunidadeFragment(){
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
    View view = inflater.inflate(R.layout.fragment_comunidade, container, false);
    searchView = view.findViewById(R.id.sv_treinadores);
    listView = view.findViewById(R.id.lv_treinadores);
  
    listaTreinadores = new ArrayList<>();
    
    adapter = new ListaTreinadoresAdapter(mContext);
    filtros = new Filtros(adapter, mContext);
    
    listView.setAdapter(adapter);
    
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
      @Override
      public boolean onQueryTextSubmit(String query){
        return false;
      }
  
      @Override
      public boolean onQueryTextChange(String newText){
        filtros.filtrarTreinadores(newText, listaTreinadores);
        
        if(newText.length() == 0)
          adapter.adcionarTreinadores(listaTreinadores);
        
        return true;
      }
    });
    getTreinadores();
    return view;
  }
  
  @Override
  public void onAttach(Context context){
    super.onAttach(context);
    mContext = context;
  }
  
  @Override
  public void onDetach(){
    super.onDetach();
    firebaseReference.removeEventListener(valueEventListenerTreinadores);
  }
  
  
  private void getTreinadores(){
    final UserPreferences preferencias = new UserPreferences(mContext);
    firebaseReference = ConfiguracaoFirebase.getFirebase().child("Treinadores");
    
    valueEventListenerTreinadores = new ValueEventListener(){
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot){
        
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            final Treinador treinador = data.getValue(Treinador.class);
            assert treinador != null;

            if(!treinador.getId().equals(preferencias.getIdentificador())){
              getPokemons(data.getChildren());
              treinador.setPokemons(listaPokemons);
              listaTreinadores.add(treinador);
              adapter.adcionarTreinadores(treinador);
            }
          }
          catch(Exception e){
            e.printStackTrace();
          }
          
        }
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    firebaseReference.addListenerForSingleValueEvent(valueEventListenerTreinadores);
  }
  
  private void getPokemons(Iterable<DataSnapshot> data){
    listaPokemons = new ArrayList<>();
    try{
      for(DataSnapshot d : data){
        Pokemon resposta = d.getValue(Pokemon.class);
        listaPokemons.add(resposta);
      }
    }
    
    catch(Exception e){
      e.printStackTrace();
    }
    
  }
}


