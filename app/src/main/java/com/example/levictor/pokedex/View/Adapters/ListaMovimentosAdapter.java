package com.example.levictor.pokedex.View.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.levictor.pokedex.Model.PokemonMoves;
import com.example.levictor.pokedex.R;

import java.util.ArrayList;

public class ListaMovimentosAdapter extends BaseAdapter{
  private ArrayList<PokemonMoves> dataset;
  private Context mContext;
  private TextView tvNomeMovimento;
  private TextView tvTipoMovimento;
  private TextView tvPpMovimento;
  private TextView tvAccuracyMovimento;
  private TextView tvPowerMovimento;
  private RelativeLayout rlListaMovimentos;
  
  public ListaMovimentosAdapter(Context mContext){
    this.mContext = mContext;
    dataset = new ArrayList<>();
  }
  
  @Override
  public int getCount(){
    return dataset.size();
  }
  
  @Override
  public Object getItem(int position){
    return null;
  }
  
  @Override
  public long getItemId(int position){
    return 0;
  }
  
  @Override
  public View getView(int position, View view, ViewGroup parent){
    view = View.inflate(mContext, R.layout.layout_lista_movimentos_pokemon, null);
    tvNomeMovimento = view.findViewById(R.id.tv_nome_movimento);
    tvAccuracyMovimento = view.findViewById(R.id.tv_accuracy_movimento);
    tvPowerMovimento = view.findViewById(R.id.tv_power);
    tvPpMovimento = view.findViewById(R.id.tv_pp_movimento);
    tvTipoMovimento = view.findViewById(R.id.tv_tipo_movimento);
    rlListaMovimentos = view.findViewById(R.id.rl_movimentos_pokemon);
    PokemonMoves moves = dataset.get(position);
    
    String txtAccuracy;
    String txtPower;
    String txtPp;
    String txtNome;
    String tipo;
    
    tipo = tratarNomeMovimento(moves.getType().getName());
    tvTipoMovimento.setText(tipo);
    
    switch(moves.getType().getName()){
    
      case "bug":
        tvTipoMovimento.setBackgroundResource(R.drawable.bug_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.bug_type);
        break;
      case "dark":
        tvTipoMovimento.setBackgroundResource(R.drawable.dark_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.dark_type);
        break;
      case "dragon":
        tvTipoMovimento.setBackgroundResource(R.drawable.dragon_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.dragon_type);
        break;
      case "electric":
        tvTipoMovimento.setBackgroundResource(R.drawable.electric_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.electric_type);
        break;
      case "fairy":
        tvTipoMovimento.setBackgroundResource(R.drawable.fairy_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.fairy_type);
        break;
      case "fighting":
        tvTipoMovimento.setBackgroundResource(R.drawable.fighting_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.fighting_type);
        break;
      case "fire":
        tvTipoMovimento.setBackgroundResource(R.drawable.fire_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.fire_type);
        break;
      case "flying":
        tvTipoMovimento.setBackgroundResource(R.drawable.flying_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.flying_type);
        break;
      case "ghost":
        tvTipoMovimento.setBackgroundResource(R.drawable.ghost_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.ghost_type);
        break;
      case "grass":
        tvTipoMovimento.setBackgroundResource(R.drawable.grass_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.grass_type);
        break;
      case "ground":
        tvTipoMovimento.setBackgroundResource(R.drawable.ground_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.ground_type);
        break;
      case "ice":
        tvTipoMovimento.setBackgroundResource(R.drawable.ice_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.ice_type);
        break;
      case "normal":
        tvTipoMovimento.setBackgroundResource(R.drawable.normal_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.normal_type);
        break;
      case "poison":
        tvTipoMovimento.setBackgroundResource(R.drawable.poison_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.poison_type);
        break;
      case "psychic":
        tvTipoMovimento.setBackgroundResource(R.drawable.psychic_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.psychic_type);
        break;
      case "rock":
        tvTipoMovimento.setBackgroundResource(R.drawable.rock_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.rock_type);
        break;
      case "steel":
        tvTipoMovimento.setBackgroundResource(R.drawable.steel_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.steel_type);
        break;
      case "water":
        tvTipoMovimento.setBackgroundResource(R.drawable.water_type);
        rlListaMovimentos.setBackgroundResource(R.drawable.water_type);
        break;
    }
    
    txtAccuracy = "Accuracy: "+moves.getAccuracy();
    txtNome = tratarNomeMovimento(moves.getName());
    txtPower = "Power: " +moves.getPower();
    txtPp = "PP: "+moves.getPp();
    
    tvAccuracyMovimento.setText(txtAccuracy);
    tvNomeMovimento.setText(txtNome);
    tvPowerMovimento.setText(txtPower);
    tvPpMovimento.setText(txtPp);
    
    return view;
  }
  
  public void adcionarListaMovimentos(PokemonMoves movimento){
  
    try{
      dataset.add(movimento);
      notifyDataSetChanged();
  
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  
  public static String tratarNomeMovimento(String text){
    char[] letters = text.toCharArray();
    letters[0] = Character.toUpperCase(letters[0]);
    for (int i = 0; i < letters.length; i++){
      if (letters[i] == '-') {
        letters[i] = ' ';
        letters[i+1] = Character.toUpperCase(letters[i+1]);
      }
    }
    
    return new String(letters);
  }
}
