package com.example.levictor.pokedex.Model;

import java.util.ArrayList;

public class PokemonMoves{
  private int accuracy;
  private int pp;
  private int power;
  private String name;
  private int movesId;
  private PokemonType type;
  private PokemonMove move;
  
  public int getMovesId(){
    String[] urlPartida = move.getUrl().split("/");
    return Integer.parseInt(urlPartida[urlPartida.length - 1]);
  }
  
  public void setMovesId(int movesId){
    this.movesId = movesId;
  }
  
  public PokemonMove getMove(){
    return move;
  }
  
  public void setMove(PokemonMove move){
    this.move = move;
  }
  
  public int getAccuracy(){
    return accuracy;
  }
  
  public void setAccuracy(int accuracy){
    this.accuracy = accuracy;
  }
  
  public int getPp(){
    return pp;
  }
  
  public void setPp(int pp){
    this.pp = pp;
  }
  
  public int getPower(){
    return power;
  }
  
  public void setPower(int power){
    this.power = power;
  }
  
  public String getName(){
    return name;
  }
  
  public void setName(String name){
    this.name = name;
  }
  
  public PokemonType getType(){
    return type;
  }
  
  public void setType(PokemonType type){
    this.type = type;
  }
}
