package com.example.levictor.pokedex.Helper;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.View.Adapters.ListaPokemonAdapter;
import com.example.levictor.pokedex.View.Adapters.ListaTreinadoresAdapter;

import java.util.ArrayList;

public class Filtros{
  final String TAG = "POKEDEX";
  private ListaPokemonAdapter adapterPokemon;
  private ListaTreinadoresAdapter adapterTreinadores;
  private Context mContext;
  
  public Filtros(ListaPokemonAdapter adapterPokemon, Context context){
    this.adapterPokemon = adapterPokemon;
    this.mContext = context;
  }
  
  public Filtros(ListaTreinadoresAdapter adapter, Context context){
    this.adapterTreinadores = adapter;
    this.mContext = context;
  }
  
  public static String tratarNomes(String text){
    char[] letters = text.toCharArray();
    letters[0] = Character.toUpperCase(letters[0]);
    for(int i = 0; i < letters.length; i++){
      if(letters[i] == ' '){
        letters[i + 1] = Character.toUpperCase(letters[i + 1]);
      }
    }
    return new String(letters);
  }
  
  //  LISTA DE ACORDO COM O TIPO DOS POKEMONS
  public void filtrarPokemons(String type, TextView tvCancelarFiltro, final ArrayList<Pokemon> listaPokemon){
    type = type.toLowerCase();
    ArrayList<Pokemon> listaPokemonModificada = new ArrayList<>();
    for(Pokemon p : listaPokemon){
      if(p.getTypes().get(0).getType().getName().equals(type)){
        listaPokemonModificada.add(p);
      }
      if(p.getTypes().size() == 2){
        if(p.getTypes().get(1).getType().getName().equals(type)){
          listaPokemonModificada.add(p);
        }
      }
      tvCancelarFiltro.setVisibility(View.VISIBLE);
    }
    adapterPokemon.adcionarPokemon(listaPokemonModificada);
  }
  
  //  LISTA DE ACORDO COM O NOME DOS POKEMONS
  public void filtrarPokemons(String nome, final ArrayList<Pokemon> listaPokemon){
    nome = nome.toLowerCase();
    ArrayList<Pokemon> listaPokemonModificada = new ArrayList<>();
    for(Pokemon p : listaPokemon){
      if(p.getName().contains(nome)){
        listaPokemonModificada.add(p);
      }
      adapterPokemon.adcionarPokemon(listaPokemonModificada);
    }
  }
  
  public void filtrarTreinadores(String nome, final ArrayList<Treinador> treinadores){
    try{
      ArrayList<Treinador> listaTreinadoresModificada = new ArrayList<>();
      nome = nome.toLowerCase();
      nome = tratarNomes(nome);
      for(Treinador t : treinadores){
        if(t.getNome().contains(nome)){
          listaTreinadoresModificada.add(t);
        }
      }
      adapterTreinadores.adcionarTreinadores(listaTreinadoresModificada);
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
}
