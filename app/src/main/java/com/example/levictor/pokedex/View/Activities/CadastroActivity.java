package com.example.levictor.pokedex.View.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Helper.Base64Custom;
import com.example.levictor.pokedex.Helper.UserPreferences;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class CadastroActivity extends AppCompatActivity{
  private EditText etNome;
  private EditText etEmail;
  private EditText etSenha;
  private Button btnCadastrar;
  private Treinador treinador;
  private ImageView imgMasculino;
  private ImageView imgFeminino;
  private static final String TAG = "POKEDEX";
  private FirebaseAuth autentificacao;
  private DatabaseReference firebaseReference;
  
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cadastro);
    initView();
    treinador = new Treinador();
    imgFeminino.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        imgFeminino.setBackgroundResource(R.drawable.water_type);
        imgMasculino.setBackgroundColor(0);
        treinador.setGenero(2);
      }
    });
    imgMasculino.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        imgMasculino.setBackgroundResource(R.drawable.water_type);
        imgFeminino.setBackgroundColor(0);
        treinador.setGenero(4);
      }
    });
    btnCadastrar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(treinador.getGenero() == 0){
          Toast.makeText(CadastroActivity.this, "Por favor, selecione seu genero", Toast.LENGTH_SHORT).show();
        }
        else if(etNome.getText().toString().length() == 0){
          Toast.makeText(CadastroActivity.this, "Por favor, preencha seu nome", Toast.LENGTH_SHORT).show();
  
        }
        else{
  
          treinador.setNome(etNome.getText().toString());
          treinador.setEmail(etEmail.getText().toString());
          treinador.setSenha(etSenha.getText().toString());
  
          cadastrarUsuario();
        }
      }
    });
  }
  
  private void initView(){
    etNome = findViewById(R.id.et_nome_cadastro);
    etEmail = findViewById(R.id.et_email_cadastro);
    etSenha = findViewById(R.id.et_senha_cadastro);
    btnCadastrar = findViewById(R.id.btn_cadastrar);
    imgFeminino = findViewById(R.id.img_genero_feminino);
    imgMasculino = findViewById(R.id.img_genero_masculino);
  }
  
  private void cadastrarUsuario(){
    autentificacao= ConfiguracaoFirebase.getFirebaseAuth();
    autentificacao.createUserWithEmailAndPassword(
        treinador.getEmail(),
        treinador.getSenha())
                  .addOnCompleteListener(CadastroActivity.this, new OnCompleteListener<AuthResult>() {
      @Override
      public void onComplete(@NonNull Task<AuthResult> task) {
        
        if (task.isSuccessful()){
          Toast.makeText(CadastroActivity.this,"Cadastro realizado com sucesso!",Toast.LENGTH_SHORT).show();
          
          FirebaseUser usuarioFirebase = task.getResult().getUser();
          
          String identificador= Base64Custom.codficarBase64(treinador.getEmail());
          
          
          treinador.setId(identificador);
          treinador.salvar();
          Log.i(TAG, identificador);
          Log.i(TAG, treinador.getNome());
          UserPreferences preferencias = new UserPreferences(CadastroActivity.this);
          preferencias.salvarDados(identificador, treinador.getNome());
          firebaseReference = ConfiguracaoFirebase.getFirebase().child("Treinadores").child(identificador);
          firebaseReference.setValue(treinador);
          Intent intent = new Intent(CadastroActivity.this, LoginActivity.class);
          startActivity(intent);
          finish();
        }
        
        else{
          
          String erroExcessao="";
          
          try {
            throw task.getException();
          }
          catch (FirebaseAuthWeakPasswordException e){
            erroExcessao="Digite uma senha mais forte, contendo mais caracteres!";
          }
          
          catch (FirebaseAuthInvalidCredentialsException e) {
            erroExcessao="E-mail invalido!";
          }
          catch (FirebaseAuthUserCollisionException e) {
            erroExcessao="Esse e-mail ja está em uso!";
          }
          catch (Exception e) {
            erroExcessao="Erro ao efetuar o cadastro!";
            e.printStackTrace();
          }
          
          Toast.makeText(CadastroActivity.this, "Erro: "+erroExcessao, Toast.LENGTH_SHORT).show();
        }
      }
    });
  }
}
