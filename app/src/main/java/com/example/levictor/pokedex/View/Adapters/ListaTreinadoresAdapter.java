package com.example.levictor.pokedex.View.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.levictor.pokedex.View.Activities.DetalhesTreinadorActivity;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;

import java.util.ArrayList;

public class ListaTreinadoresAdapter extends BaseAdapter{
  private Context mContext;
  private ArrayList<Treinador> dataset;
  private TextView tvNomeTreinador;
  private TextView tvNumeroPokemons;
  private ImageView ivGenero;
  
  public ListaTreinadoresAdapter(Context mContext){
    this.mContext = mContext;
    dataset = new ArrayList<>();
  }
  
  @Override
  public int getCount(){
    return dataset.size();
  }
  
  @Override
  public Object getItem(int position){
    return null;
  }
  
  @Override
  public long getItemId(int position){
    return 0;
  }
  
  @Override
  public View getView(final int position, View view, ViewGroup parent){
    view = View.inflate(mContext, R.layout.layout_lista_treinadores, null);
    tvNomeTreinador = view.findViewById(R.id.tv_nome_treinador);
    tvNumeroPokemons = view.findViewById(R.id.tv_numero_pokemons_treinador);
    ivGenero = view.findViewById(R.id.iv_sexo_treinador);
    
    if(dataset.get(position).getGenero() == 2){
      ivGenero.setBackgroundResource(R.drawable.character_female);
    }
    if(dataset.get(position).getGenero() == 4){
      ivGenero.setBackgroundResource(R.drawable.character_male);
    }
    
    String numeroPokemons;
    if(dataset.get(position).getPokemons() == null){
      numeroPokemons = "Número de pokemons: 0";
    }
    else{
      numeroPokemons = "Número de pokemons: " + dataset.get(position).getPokemons().size();
    }
    tvNumeroPokemons.setText(numeroPokemons);
    
    String nomeTreinador = dataset.get(position).getNome();
    tvNomeTreinador.setText(nomeTreinador);
    
    view.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        Intent intent = new Intent(mContext, DetalhesTreinadorActivity.class);
        intent.putExtra("idTreinador", dataset.get(position).getId());
        mContext.startActivity(intent);
      }
    });
    
    return view;
  }
  
  public void adcionarTreinadores(Treinador treinadores){
    
    try{
      dataset.add(treinadores);
      notifyDataSetChanged();
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  public void adcionarTreinadores(ArrayList<Treinador> treinadores){
    
    try{
      dataset.clear();
      dataset.addAll(treinadores);
      notifyDataSetChanged();
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
}
