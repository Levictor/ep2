package com.example.levictor.pokedex.View.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Helper.Base64Custom;
import com.example.levictor.pokedex.Helper.UserPreferences;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity{
  private TextView tvCadastro;
  private Button botaoLogar;
  private EditText etEmail;
  private EditText etSenha;
  private Treinador treinador;
  private DatabaseReference firebaseReference;
  private FirebaseAuth auth;
  private ValueEventListener valueEventListener;
  private String identificador;
  
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    initView();
    
    tvCadastro.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        Intent intent = new Intent(LoginActivity.this, CadastroActivity.class);
        startActivity(intent);
      }
    });
    verificarUsuarioLogado();
    
    firebaseReference = ConfiguracaoFirebase.getFirebase();
    
    
    botaoLogar.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        treinador = new Treinador();
        treinador.setEmail(etEmail.getText().toString());
        treinador.setSenha(etSenha.getText().toString());
        
        if(etEmail.getText().toString().length() != 0 && etSenha.getText().toString().length() != 0){
          validarLogin();
        }
      }
      
    });
  }
  
  private void initView(){
    tvCadastro = findViewById(R.id.tv_cadastro);
    etEmail = findViewById(R.id.et_email);
    etSenha = findViewById(R.id.et_senha);
    botaoLogar = findViewById(R.id.btn_login);
  
  }
  
  private void verificarUsuarioLogado(){
    auth = ConfiguracaoFirebase.getFirebaseAuth();
    if(auth.getCurrentUser() != null){
      Intent intent = new Intent(LoginActivity.this, MainActivity.class);
      startActivity(intent);
      finish();
    }
  }
  
  private void validarLogin(){
    auth = ConfiguracaoFirebase.getFirebaseAuth();
    auth.signInWithEmailAndPassword(treinador.getEmail(), treinador.getSenha())
        .addOnCompleteListener(new OnCompleteListener<AuthResult>(){
          @Override
          public void onComplete(@NonNull Task<AuthResult> task){
        
            if(task.isSuccessful()){
          
              identificador = Base64Custom.codficarBase64(treinador.getEmail());
          
              firebaseReference = ConfiguracaoFirebase.getFirebase().child("Treinadores").child(identificador);
          
              valueEventListener = new ValueEventListener(){
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot){
                  try{
                    Treinador usuarioRecuperado = dataSnapshot.getValue(Treinador.class);
                
                    UserPreferences preferencias = new UserPreferences(LoginActivity.this);
                    preferencias.salvarDados(identificador, usuarioRecuperado.getNome());
                
                  }
                  catch(Exception e){
                    e.printStackTrace();
                  }
                }
            
                @Override
                public void onCancelled(DatabaseError databaseError){
              
                }
              };
              firebaseReference.addListenerForSingleValueEvent(valueEventListener);
          
          
              Toast.makeText(LoginActivity.this, "Sucesso ao fazer login", Toast.LENGTH_SHORT).show();
          
              Intent intent = new Intent(getApplicationContext(), MainActivity.class);
              startActivity(intent);
              finish();
            }
            else{
              Toast.makeText(LoginActivity.this, "Erro ao fazer login", Toast.LENGTH_SHORT).show();
          
            }
          }
        });
  }
  
  
}
