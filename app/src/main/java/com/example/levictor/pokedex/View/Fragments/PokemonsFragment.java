package com.example.levictor.pokedex.View.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.levictor.pokedex.Helper.Filtros;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Pokeapi.PokeapiService;
import com.example.levictor.pokedex.R;
import com.example.levictor.pokedex.View.Adapters.ListaPokemonAdapter;
import com.example.levictor.pokedex.View.Adapters.SpinnerTiposAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.levictor.pokedex.Helper.SystemConstants.FRAGMENT_POKEDEX;


public class PokemonsFragment extends Fragment{
  
  private static final String TAG = "POKEDEX";
  
  public ArrayList<Pokemon> listaPokemon = new ArrayList<>();
  
  private Retrofit retrofit;
  
  private ArrayList<String> list;
  
  private ListaPokemonAdapter adapter;
  private SpinnerTiposAdapter adapterSpinner;
  
  private Context mContext;
  private RecyclerView recyclerView;
  private TextView tvCancelarFiltro;
  private SearchView searchView;
  private Spinner spinner;
  
  private Filtros filtros;
  
  private boolean condicaoFiltroTipo = false;
  private String tipoFiltro;
  
  private boolean condicaoFiltroNome = false;
  private String nomeDigitado;
  
  public PokemonsFragment(){
    // Required empty public constructor
  }
  
  @Override
  public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState){
    View view = inflater.inflate(R.layout.fragment_pokemons, container, false);
    initView(view);
    preencherListaSpinner();
    
    retrofit = new Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .build();
    
    adapter = new ListaPokemonAdapter(mContext, FRAGMENT_POKEDEX);
    filtros = new Filtros(adapter, mContext);
    
    recyclerView.setAdapter(adapter);
    recyclerView.setHasFixedSize(true);
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
    recyclerView.setLayoutManager(layoutManager);
    
    
    adapterSpinner = new SpinnerTiposAdapter(mContext, list);
    spinner.setAdapter(adapterSpinner);
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
      @Override
      public void onItemSelected(final AdapterView<?> parent, View view, int position, long id){
        final TextView t1 = view.findViewById(R.id.tv_spinner_tipo_1);
        final TextView t2 = view.findViewById(R.id.tv_spinner_tipo_2);
        final TextView t3 = view.findViewById(R.id.tv_spinner_tipo_3);
        
        t1.setOnClickListener(new View.OnClickListener(){
          @Override
          public void onClick(View v){
            condicaoFiltroTipo = true;
            tipoFiltro = t1.getText().toString();
            filtros.filtrarPokemons(tipoFiltro, tvCancelarFiltro, listaPokemon);
          }
        });
        t2.setOnClickListener(new View.OnClickListener(){
          @Override
          public void onClick(View v){
            condicaoFiltroTipo = true;
            tipoFiltro = t2.getText().toString();
            filtros.filtrarPokemons(tipoFiltro, tvCancelarFiltro, listaPokemon);
          }
        });
        t3.setOnClickListener(new View.OnClickListener(){
          @Override
          public void onClick(View v){
            condicaoFiltroTipo = true;
            tipoFiltro = t3.getText().toString();
            filtros.filtrarPokemons(tipoFiltro, tvCancelarFiltro, listaPokemon);
          }
        });
      }
      
      @Override
      public void onNothingSelected(AdapterView<?> parent){
      
      }
    });
    
    tvCancelarFiltro.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        tipoFiltro = null;
        condicaoFiltroTipo = false;
        adapter.adcionarPokemon(listaPokemon);
        tvCancelarFiltro.setVisibility(View.GONE);
      }
    });
    
    
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
      @Override
      public boolean onQueryTextSubmit(String s){
        return false;
      }
      
      @Override
      public boolean onQueryTextChange(String s){
        condicaoFiltroNome = true;
        condicaoFiltroTipo = false;
        nomeDigitado = s;
        tvCancelarFiltro.setVisibility(View.GONE);
        filtros.filtrarPokemons(s, listaPokemon);
        return true;
      }
    });
    
    listarPokemons(0);
    listarPokemons(3000);
    listarPokemons(6000);
    listarPokemons(9000);
    listarPokemons(12000);
    
    return view;
  }
  
  private void listarPokemons(final int time){
    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        if(verificarConexao() && listaPokemon.size() == 0){
          for(int i = 1; i <= 802; i++){
            requerirPokemon(i);
          }
          for(int i = 10001; i <= 10090; i++){
            requerirPokemon(i);
          }
          
        }
        
        else if(!verificarConexao()){
          Toast.makeText(mContext, "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show();
        }
        
        
      }
    }, time);
  }
  
  private boolean verificarConexao(){
    
    ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context
                                                                                                  .CONNECTIVITY_SERVICE);
    
    assert connectivityManager != null;
    if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                          .getState() == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(
        ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED){
      return true;
    }

    else{
      return false;
    }
    
  }
  
  private void requerirPokemon(int id){
    PokeapiService service = retrofit.create(PokeapiService.class);
    Call<Pokemon> respostaCall = service.obterPokemons(id);
    respostaCall.enqueue(new Callback<Pokemon>(){
      @Override
      public void onResponse(Call<Pokemon> call, Response<Pokemon> response){
        if(response.isSuccessful()){
          setListaPokemon(response.body());
        }
        else{
          call.cancel();
          Log.i(TAG, "onResponse: " + response.errorBody());
        }
      }
      
      @Override
      public void onFailure(Call<Pokemon> call, Throwable t){
        Log.i(TAG, "onFalilure: " + t.getMessage());
      }
    });
  }
  
  private void initView(View view){
    recyclerView = view.findViewById(R.id.rv_pokemons);
    tvCancelarFiltro = view.findViewById(R.id.tv_cancelar_filtro);
    searchView = view.findViewById(R.id.search_id);
    spinner = view.findViewById(R.id.spinner_tipo);
  }
  
  public void setListaPokemon(Pokemon pokemon){
    this.listaPokemon.add(pokemon);
    
    if(condicaoFiltroTipo){
      filtros.filtrarPokemons(tipoFiltro, tvCancelarFiltro, listaPokemon);
    }
    else if(condicaoFiltroNome){
      filtros.filtrarPokemons(nomeDigitado, listaPokemon);
    }
    else{
      adapter.adcionarPokemon(pokemon);
    }
  }
  
  private void preencherListaSpinner(){
    list = new ArrayList<>();
    list.add("a");
    list.add("a");
    list.add("a");
    list.add("a");
    list.add("a");
    list.add("a");
  }
  
  @Override
  public void onAttach(Context context){
    super.onAttach(context);
    mContext = context;
  }
}
