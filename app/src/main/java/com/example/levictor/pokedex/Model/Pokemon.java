package com.example.levictor.pokedex.Model;

import java.util.ArrayList;

public class Pokemon{
  private ArrayList<PokemonTypes> types;
  private String name;
  private int id;
  
  public String getName(){
    return name;
  }
  
  public void setName(String name){
    this.name = name;
  }
  
  public int getId(){
    return id;
  }
  
  public void setId(int id){
    this.id = id;
  }
  
  public ArrayList<PokemonTypes> getTypes(){
    return types;
  }
  
  public void setTypes(ArrayList<PokemonTypes> types){
    this.types = types;
  }

}
