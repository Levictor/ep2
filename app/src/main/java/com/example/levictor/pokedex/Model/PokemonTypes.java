package com.example.levictor.pokedex.Model;

public class PokemonTypes{
  private int slot;
  private PokemonType type;
  
  public int getSlot(){
    return slot;
  }
  
  public void setSlot(int slot){
    this.slot = slot;
  }
  
  public PokemonType getType(){
    return type;
  }
  
  public void setType(PokemonType type){
    this.type = type;
  }
}
