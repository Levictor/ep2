package com.example.levictor.pokedex.View.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Helper.UserPreferences;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.PokemonDetails;
import com.example.levictor.pokedex.Model.PokemonMoves;
import com.example.levictor.pokedex.Model.PokemonTypes;
import com.example.levictor.pokedex.Pokeapi.PokeapiService;
import com.example.levictor.pokedex.R;
import com.example.levictor.pokedex.View.Adapters.ListaMovimentosAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.levictor.pokedex.Helper.SystemConstants.*;

public class PokemonDetalhesActivity extends AppCompatActivity{
  
  private static final String TAG = "POKEDEX";
  private PokemonDetails pokemon;
  private ImageView ivPokemonNormal;
  private ImageView ivPokemonShiny;
  private ListView lvMovimentos;
  private RelativeLayout rlPokemonDetalhes;
  private ListaMovimentosAdapter adapter;
  private Button btnAdd;
  private LinearLayout llPokemonShiny;
  private LinearLayout llPokemonNormal;
  private GradientDrawable gradientDrawable;
  
  private Retrofit retrofit;
  private int idPokemon;
  
  private String origemActivity;
  
  private String cor1;
  private String cor2;
  
  //  TRUE = O TREINADOR JÁ POSSUI
  //  FALSE = O TREINADOR NAO POSSUI
  private boolean condicaoPokemon;
  private String idPokemonFirebase;
  private Context mContext;
  private DatabaseReference firebaseReference;
  private ValueEventListener valueEventListenerTreinadores;
  private ArrayList<Pokemon> listaPokemons = new ArrayList<>();
  private boolean condicaoCarregamento;
  private Button btnRemover;
  private UserPreferences preferences;
  
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pokemon_detalhes);
    
    initView();
    createListeners();
    
    ActionBar actionBar = getSupportActionBar();
    assert actionBar != null;
    actionBar.hide();
    
    
    condicaoCarregamento =  true;
    
    Intent intent = getIntent();
    if(intent.getExtras() != null){
      idPokemon = intent.getIntExtra("idPokemon", 0);
      origemActivity = intent.getStringExtra("fonte");
    }
    
    mContext = this;
    preferences = new UserPreferences(mContext);
    retrofit = new Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .build();
    
    
    switch(origemActivity){
      case FRAGMENT_PERFIL:
        btnAdd.setVisibility(View.GONE);
        break;
      case FRAGMENT_POKEDEX:
        btnRemover.setVisibility(View.GONE);
        break;
      
      case ACTIVITY_DETALHES_TREINADOR:
        btnRemover.setVisibility(View.GONE);
        break;
    }
    
    Glide.with(this)
         .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + idPokemon + ".png")
         .centerCrop()
         .crossFade()
         .diskCacheStrategy(DiskCacheStrategy.ALL)
         .into(ivPokemonNormal);
    
    Glide.with(this)
         .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" + idPokemon + ".png")
         .centerCrop()
         .crossFade()
         .diskCacheStrategy(DiskCacheStrategy.ALL)
         .into(ivPokemonShiny);
    
    requestPokemon(idPokemon);
    adapter = new ListaMovimentosAdapter(mContext);
    lvMovimentos.setAdapter(adapter);


    
    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        if(ivPokemonShiny.getDrawable() == null){
          DisplayMetrics metrics = getResources().getDisplayMetrics();
  
          int deviceTotalWidth = metrics.widthPixels;
          llPokemonShiny.setVisibility(View.GONE);
          llPokemonNormal.setX(deviceTotalWidth/4);
        }
        recuperarListaTreinador();
      }
    }, 2000);
    
    setBackgroundColors(1000);
    setBackgroundColors(2000);
    setBackgroundColors(3000);
    setBackgroundColors(4000);
    
  }
  
  private void createListeners(){
    btnAdd.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        try{
          if(!condicaoCarregamento && !condicaoPokemon){
            firebaseReference = ConfiguracaoFirebase.getFirebase().child("Treinadores").child(preferences.getIdentificador());
            firebaseReference.push().setValue(pokemon);
            Toast.makeText(mContext, "Pokemon adcionado", Toast.LENGTH_SHORT).show();
          }
          else if(condicaoCarregamento){
            Toast.makeText(mContext, "Aguarde para adcionar o pokemon", Toast.LENGTH_SHORT).show();
          }
          else{
            abrirDialog();
          }
        
        }
        catch(Exception e){
          Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show();
          e.printStackTrace();
        }
      }
    });
  
    btnRemover.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        try{
          if(!condicaoCarregamento){
            firebaseReference = ConfiguracaoFirebase.getFirebase()
                                                    .child("Treinadores")
                                                    .child(preferences.getIdentificador())
                                                    .child(idPokemonFirebase);
            firebaseReference.removeValue();
            Toast.makeText(mContext, "Pokemon removido", Toast.LENGTH_SHORT).show();
          }
          else{
            Toast.makeText(mContext, "Aguarde para remover o pokemon", Toast.LENGTH_SHORT).show();
          }
        }
        catch(Exception e){
          Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show();
          e.printStackTrace();
        }
      }
    });
  
  }
  
  private void initView(){
    ivPokemonNormal = findViewById(R.id.iv_pokemon_normal);
    ivPokemonShiny = findViewById(R.id.iv_pokemon_shiny);
    lvMovimentos = findViewById(R.id.lv_movimentos);
    btnAdd = findViewById(R.id.btn_add);
    btnRemover = findViewById(R.id.btn_cancel);
    llPokemonShiny = findViewById(R.id.ll_pokemon_shiny);
    llPokemonNormal = findViewById(R.id.ll_pokemon_normal);
    
  }
  
  public void setBackgroundColors(final int time){
    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        try{
          for(PokemonTypes p : pokemon.getTypes()){
            String cor = "";
            
            switch(p.getType().getName()){
              case BUG_TYPE:
                cor = "#A8B820";
                break;
              case DARK_TYPE:
                cor = "#705848";
                break;
              case DRAGON_TYPE:
                cor = "#4924A1";
                break;
              case ELECTRIC_TYPE:
                cor = "#F8D030";
                break;
              case FAIRY_TYPE:
                cor = "#EE99AC";
                break;
              case FIGHTING_TYPE:
                cor = "#7D1F1A";
                break;
              case FIRE_TYPE:
                cor = "#9C531F";
                break;
              case FLYING_TYPE:
                cor = "#6D5E9C";
                break;
              case GHOST_TYPE:
                cor = "#705898";
                break;
              case GRASS_TYPE:
                cor = "#4E8234";
                break;
              case GROUND_TYPE:
                cor = "#E0C068";
                break;
              case ICE_TYPE:
                cor = "#98D8D8";
                break;
              case NORMAL_TYPE:
                cor = "#6D6D4E";
                break;
              case POISON_TYPE:
                cor = "#682A68";
                break;
              case PSYCHIC_TYPE:
                cor = "#F85888";
                break;
              case ROCK_TYPE:
                cor = "#B8A038";
                break;
              case STEEL_TYPE:
                cor = "#B8B8D0";
                break;
              case WATER_TYPE:
                cor = "#445E9C";
                break;
            }
            if(p.getSlot() == 1){
              cor1 = cor;
            }
            else{
              cor2 = cor;
            }
          }
          rlPokemonDetalhes = findViewById(R.id.rl_activity_pokemon_detalhes);
          if(cor2 != null){
            gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR,
                                                    new int[]{Color.parseColor(cor1), Color.parseColor(cor2)});
          }
          else{
            gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR,
                                                    new int[]{Color.parseColor(cor1), Color.parseColor("#000000")});
          }
          rlPokemonDetalhes.setBackgroundDrawable(gradientDrawable);
        }
        catch(Exception e){
          if(time == 4000)
            Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show();
          
          e.printStackTrace();
        }
      }
    }, time);
  }
  
  private void abrirDialog(){
    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
    View mView = getLayoutInflater().inflate(R.layout.layout_dialog_repetir_pokemon, null);
    mView.setBackgroundColor(Color.parseColor("#666666"));
    
    Button btnAdcionar = mView.findViewById(R.id.btn_adcionar);
    Button btnCancelar = mView.findViewById(R.id.btn_cancelar);
    TextView tvNomePokemon = mView.findViewById(R.id.tv_pokemon_repetido);
    ImageView ivPokemonRepetido = mView.findViewById(R.id.iv_pokemon_repetido);
    
    Glide.with(this)
         .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + idPokemon + ".png")
         .centerCrop()
         .crossFade()
         .diskCacheStrategy(DiskCacheStrategy.ALL)
         .into(ivPokemonRepetido);
    
    String txtPerguntaPokemon = "Você já possui o(a) " + tratarNomes(pokemon.getName()) + " deseja adcionar-lo(a) novamente?";
    
    tvNomePokemon.setText(txtPerguntaPokemon);
    
    builder.setView(mView);
    final AlertDialog dialog = builder.create();
    dialog.show();
    btnCancelar.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        dialog.cancel();
      }
    });
    btnAdcionar.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v){
        firebaseReference.push().setValue(pokemon);
        Toast.makeText(mContext, "Pokemon adcionado", Toast.LENGTH_SHORT).show();
        dialog.cancel();
      }
    });
  }
  
  private void requestPokemon(int i){
    PokeapiService service = retrofit.create(PokeapiService.class);
    final Call<PokemonDetails> respostaCall = service.obterDetalhesPokemons(i);
    respostaCall.enqueue(new Callback<PokemonDetails>(){
      @Override
      public void onResponse(Call<PokemonDetails> call, Response<PokemonDetails> response){
        if(response.isSuccessful()){
          setPokemon(response.body());
        }
        else{
          Log.i(TAG, "onResponse: " + response.errorBody());
        }
      }
      
      @Override
      public void onFailure(Call<PokemonDetails> call, Throwable t){
        Log.i(TAG, "onFalilure: " + t.getMessage());
      }
    });
  }
  
  private void setPokemon(PokemonDetails pokemon){
    this.pokemon = pokemon;
    try{
      for(PokemonMoves c : pokemon.getMoves()){
        requestMoves(c.getMovesId());
      }
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }
  
  
  private void requestMoves(int id){
    PokeapiService service = retrofit.create(PokeapiService.class);
    Call<PokemonMoves> movesCall = service.obterMovimentosPokemon(id);
    movesCall.enqueue(new Callback<PokemonMoves>(){
      @Override
      public void onResponse(Call<PokemonMoves> call, Response<PokemonMoves> response){
        if(response.isSuccessful()){
          adapter.adcionarListaMovimentos(response.body());
        }
        else{
          Log.i(TAG, "onResponse: " + response.errorBody());
        }
      }
      
      @Override
      public void onFailure(Call<PokemonMoves> call, Throwable t){
        Log.i(TAG, "onFalilure: " + t.getMessage());
      }
    });
  }
  
  private void recuperarListaTreinador(){
    
    firebaseReference = ConfiguracaoFirebase.getFirebase().child("Treinadores").child(preferences.getIdentificador());
    
    valueEventListenerTreinadores = new ValueEventListener(){
      @Override
      public void onDataChange(DataSnapshot dataSnapshot){
        
        listaPokemons.clear();
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            Pokemon p = data.getValue(Pokemon.class);
            assert p != null;
            if(p.getId() == pokemon.getId()){
              condicaoPokemon = true;
              idPokemonFirebase = data.getKey();
            }
          }
          catch(Exception e){
            e.printStackTrace();
          }
          
        }
        condicaoCarregamento = false;
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    firebaseReference.addValueEventListener(valueEventListenerTreinadores);
  }
  
  @Override
  protected void onStop(){
    super.onStop();
    if(valueEventListenerTreinadores != null){
      firebaseReference.removeEventListener(valueEventListenerTreinadores);
    }
  }
  public static String tratarNomes(String text){
    char[] letters = text.toCharArray();
    letters[0] = Character.toUpperCase(letters[0]);
    for(int i = 0; i < letters.length; i++){
      if(letters[i] == '-'){
        if(!text.equals("ho-oh"))
          letters[i] = ' ';
        
        letters[i + 1] = Character.toUpperCase(letters[i + 1]);
      }
    }
    return new String(letters);
    
  }
}
