package com.example.levictor.pokedex.Model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

public class PokemonDetails{
  private ArrayList<PokemonTypes> types;
  private String name;
  private int id;
  private ArrayList<PokemonMoves> moves;
  
  @Exclude
  public ArrayList<PokemonMoves> getMoves(){
    return moves;
  }
  
  @Exclude
  public void setMoves(ArrayList<PokemonMoves> moves){
    this.moves = moves;
  }
  
  public String getName(){
    return name;
  }
  
  public void setName(String name){
    this.name = name;
  }
  
  public int getId(){
    return id;
  }
  
  public void setId(int id){
    this.id = id;
  }
  
  public ArrayList<PokemonTypes> getTypes(){
    return types;
  }
  
  public void setTypes(ArrayList<PokemonTypes> types){
    this.types = types;
  }
}
