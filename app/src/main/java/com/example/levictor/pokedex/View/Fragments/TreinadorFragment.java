package com.example.levictor.pokedex.View.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.levictor.pokedex.Config.ConfiguracaoFirebase;
import com.example.levictor.pokedex.Helper.UserPreferences;
import com.example.levictor.pokedex.Model.Pokemon;
import com.example.levictor.pokedex.Model.Treinador;
import com.example.levictor.pokedex.R;
import com.example.levictor.pokedex.View.Adapters.ListaPokemonAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.levictor.pokedex.Helper.SystemConstants.FRAGMENT_PERFIL;

public class TreinadorFragment extends Fragment{
  
  private static final String TAG = "POKEDEX";
  public ArrayList<Pokemon> listaDetalhesPokemon;
  private RecyclerView recyclerView;
  private ImageView imgGenero;
  private TextView tvNome;
  private ListaPokemonAdapter adapter;
  private Pokemon detalhesPokemon;
  private Context mContext;
  private DatabaseReference databaseReferenceTreinador;
  private DatabaseReference databaseReferencePokemons;
  private ValueEventListener valueEventListenerTreinadores;
  private ValueEventListener valueEventListenerPokemon;
  
  public TreinadorFragment(){
  }
  
  @Override
  public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
    View view = inflater.inflate(R.layout.fragment_treinador_perfil, container, false);
    initView(view);
    
    listaDetalhesPokemon = new ArrayList<>();
    getTreinador(0);
    getTreinador(3000);
    getTreinador(6000);
    getTreinador(9000);
    getTreinador(12000);
    getPokemons(0);
    getPokemons(3000);
    getPokemons(6000);
    getPokemons(9000);
    getPokemons(1200);
    adapter = new ListaPokemonAdapter(mContext, FRAGMENT_PERFIL);
    
    recyclerView.setAdapter(adapter);
    recyclerView.setHasFixedSize(true);
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
    recyclerView.setLayoutManager(layoutManager);
    return view;
  }
  
  private void getPokemons(long time){
    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        if(verificarConexao()){
          requerirPokemons();
        }
      }
    }, time);
  }
  
  private void getTreinador(long time){
    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        if(verificarConexao()){
          requerirTreinador();
        }
        else{
          Toast.makeText(mContext, "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show();
        }
      }
    }, time);
  }
  
  private void initView(View view){
    imgGenero = view.findViewById(R.id.img_genero_perfil);
    tvNome = view.findViewById(R.id.tv_nome_treinador_perfil);
    recyclerView = view.findViewById(R.id.rv_treinador);
  }
  
  private void requerirPokemons(){
    UserPreferences preferencias = new UserPreferences(mContext);
    databaseReferencePokemons = ConfiguracaoFirebase.getFirebase()
                                                    .child("Treinadores")
                                                    .child(preferencias.getIdentificador());
    
    valueEventListenerPokemon = new ValueEventListener(){
      @Override
      public void onDataChange(DataSnapshot dataSnapshot){
        
        listaDetalhesPokemon.clear();
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            Pokemon pokemon = data.getValue(Pokemon.class);
            assert pokemon != null;
            listaDetalhesPokemon.add(pokemon);
            adapter.adcionarPokemon(listaDetalhesPokemon);
          }
          catch(Exception e){
            e.printStackTrace();
          }
        }
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    databaseReferencePokemons.addValueEventListener(valueEventListenerPokemon);
  }
  
  private void requerirTreinador(){
    final UserPreferences preferencias = new UserPreferences(mContext);
    databaseReferenceTreinador = ConfiguracaoFirebase.getFirebase().child("Treinadores");
    
    valueEventListenerTreinadores = new ValueEventListener(){
      @Override
      public void onDataChange(DataSnapshot dataSnapshot){
        listaDetalhesPokemon.clear();
        for(DataSnapshot data : dataSnapshot.getChildren()){
          try{
            Treinador treinador = data.getValue(Treinador.class);
            if(treinador.getId().equals(preferencias.getIdentificador())){
              if(treinador.getGenero() == 4){
                imgGenero.setBackgroundResource(R.drawable.character_male);
              }
              else if(treinador.getGenero() == 2){
                imgGenero.setBackgroundResource(R.drawable.character_female);
              }
              tvNome.setText(treinador.getNome());
            }
          }
          catch(Exception e){
            e.printStackTrace();
          }
        }
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError){
      
      }
    };
    databaseReferenceTreinador.addValueEventListener(valueEventListenerTreinadores);
  }
  
  private boolean verificarConexao(){
    
    ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context
                                                                                                  .CONNECTIVITY_SERVICE);
    
    assert connectivityManager != null;
    if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                          .getState() == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(
        ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED){
      return true;
    }
    
    else{
      return false;
    }
    
  }
  
  
  @Override
  public void onAttach(Context context){
    super.onAttach(context);
    mContext = context;
  }
  
  @Override
  public void onDetach(){
    super.onDetach();
    if(valueEventListenerPokemon != null)
      databaseReferencePokemons.removeEventListener(valueEventListenerPokemon);
  
    if(valueEventListenerTreinadores != null)
      databaseReferenceTreinador.removeEventListener(valueEventListenerTreinadores);
    
  }
  
}
