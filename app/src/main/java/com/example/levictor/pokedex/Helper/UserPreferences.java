package com.example.levictor.pokedex.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.levictor.pokedex.Model.Pokemon;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class UserPreferences{
  
  private Context mContext;
  private SharedPreferences preferences;
  private static final String NOME_ARQUIVO="Pokedex.preferencias";
  private static final String KEY_IDENTIFICADOR="identificadorUsuarioLogado";
  private static final String KEY_NOME="nomeUsuarioLogado";
  private int MODE=0;
  private SharedPreferences.Editor editor;
  
  public UserPreferences(Context mContext){
    this.mContext = mContext;
    preferences=mContext.getSharedPreferences(NOME_ARQUIVO,MODE);
  }
  
  public void salvarDados(String identificador, String nome){
    editor = preferences.edit();
    editor.putString(KEY_IDENTIFICADOR,identificador);
    editor.putString(KEY_NOME,nome);
    editor.apply();
  }
  
  public String getIdentificador(){
    return preferences.getString(KEY_IDENTIFICADOR,null);
  }
  
  public String getNome(){
    return preferences.getString(KEY_NOME,null);
  }
}
