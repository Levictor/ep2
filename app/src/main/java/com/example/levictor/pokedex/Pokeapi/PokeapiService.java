package com.example.levictor.pokedex.Pokeapi;

import com.example.levictor.pokedex.Model.PokemonDetails;
import com.example.levictor.pokedex.Model.PokemonMoves;
import com.example.levictor.pokedex.Model.Pokemon;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokeapiService{
  
  @GET("pokemon/{id}/")
  Call<Pokemon> obterPokemons(@Path("id") int groupID);
  
  @GET("pokemon/{id}/")
  Call<PokemonDetails> obterDetalhesPokemons(@Path("id") int groupID);
  
  @GET("move/{id}/")
  Call<PokemonMoves> obterMovimentosPokemon(@Path("id") int moveId);

}
