package com.example.levictor.pokedex.Helper;

public abstract class SystemConstants{
  public static final String FRAGMENT_PERFIL = "FRAGMENT_PERFIL";
  public static final String FRAGMENT_POKEDEX = "FRAGMENT_POKEDEX";
  public static final String ACTIVITY_DETALHES_TREINADOR = "ACTIVITY_DETALHES_TREINADOR";
  public static final String BUG_TYPE = "bug";
  public static final String DARK_TYPE = "dark";
  public static final String DRAGON_TYPE = "dragon";
  public static final String ELECTRIC_TYPE = "electric";
  public static final String FAIRY_TYPE = "fairy";
  public static final String FIGHTING_TYPE = "fighting";
  public static final String FIRE_TYPE = "fire";
  public static final String FLYING_TYPE = "flying";
  public static final String GHOST_TYPE = "ghost";
  public static final String GRASS_TYPE = "grass";
  public static final String GROUND_TYPE = "ground";
  public static final String ICE_TYPE = "ice";
  public static final String NORMAL_TYPE = "normal";
  public static final String POISON_TYPE = "poison";
  public static final String PSYCHIC_TYPE = "psychic";
  public static final String ROCK_TYPE = "rock";
  public static final String STEEL_TYPE = "steel";
  public static final String WATER_TYPE = "water";
  public static final int NUMERO_MAXIMO_POKEMONS = 892;
  
}
